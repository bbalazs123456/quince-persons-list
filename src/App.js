import React, { Component } from 'react';
import Persons from './components/Persons'
import data from './data/persons.json'


class App extends Component {
  constructor(props) {
    super(props);
    this.deleteItem = this.deleteItem.bind(this);



    this.state = {
      items: [], // all items
      name: '',
      age: 0,
      nick: '',
      employee: false,
      job: '',
      isModalVisible: false,
      showMe: false
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  modalBtn = () =>{
    const { show } = this.stateclick;
    this.setState({ show : !show})
  }

  componentDidMount() {
    this.setState((prevState) => {
      return {
        items: prevState.items.concat(data)
      };
    });
  }

  handleSubmit(event) {
    const newItem = {
      name: this.state.name,
      age: this.state.age,
      nick: this.state.nick,
      employee: this.state.employee,
      job: this.state.job,
    }
    this.setState((prevState) => {
      return {
        items: prevState.items.concat(newItem),
        name: '',
        age: '',
        nick: '',
        employee: false,
        job: '',
        showMe: false
      };
    });
    event.preventDefault();
  }

  deleteItem(key) {
    const filteredItems = this.state.items.filter(function (item) {
      return(item.key !== key)
    });

    this.setState({
      items: filteredItems
    });

  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  operation(){
    this.setState({
      showMe:!this.state.showMe
    })
  }

  render() {
    return (

        <div className="container">
          <div className="project">

          {
            this.state.showMe?
            <div id="simpleModal" className="modal">
              <div id="popup" className="modal-content">

                <div class="modal-head">
                  <span>please fill out the form below!</span>
                  <span onClick={()=>this.operation()} id="closeBtn" className="closeBtn">×</span>
                </div>

                <div className="modal-article">
                  <form onSubmit={this.handleSubmit}>
                    <table>
                      <tbody>
                      <tr>
                        <td className="left-td-width">Name:</td>
                        <td>
                          <label>
                            <input
                              name="name"
                              type="text"
                              onChange={this.handleInputChange}
                              value={this.state.name} />
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td className="left-td-width">Job Title:</td>
                        <td>
                          <label>
                            <input
                              name="job"
                              type="text"
                              onChange={this.handleInputChange}
                              value={this.state.job} />
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td className="left-td-width">Age:</td>
                        <td>
                        <label>
                          <input
                            name="age"
                            type="text"
                            onChange={this.handleInputChange}
                            value={this.state.age} />
                        </label>
                        </td>
                      </tr>
                      <tr>
                        <td className="left-td-width">Nickname:</td>
                        <td>
                          <label>
                            <input
                              name="nick"
                              type="text"
                              onChange={this.handleInputChange}
                              value={this.state.nick} />
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td className="left-td-width">Employee:</td>
                        <td>
                          <label>
                            <input
                              name="employee"
                              type="checkbox"
                              onChange={this.handleInputChange}
                              checked={this.state.employee} />
                          </label>
                        </td>
                      </tr>
                    </tbody>
                    </table>

                    <div class="button-section">
                      <button id="registeredSucces" className="add-btn" type="submit">OK</button>
                      <button onClick={()=>this.operation()} id="registeredCancel" className="add-btn" type="button">CANCEL</button>
                    </div>

                  </form>
                </div>
              </div>
            </div>
              :null
          }


          <div id="personList" className="persons-list">
            <form onSubmit={this.addItem}>
              <input ref={(a) => this._inputElement = a}
                      placeholder="Enter the Names"></input>
              <button type="submit">Add</button>
            </form>
          </div>

            <button onClick={()=>this.operation()} id="modalBtn" className="add-btn" type="button">Add</button>

            <div className="person-table">
              <Persons data={this.state.items} />
            </div>
          </div>

        </div>

    );
  }
}

export default App;
