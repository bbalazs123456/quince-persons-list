import React, { Component } from "react";

class TodoItems extends Component {
    constructor(props) {
      super(props);

      this.createTasks = this.createTasks.bind(this);
    }
createTasks(item){
    return <li onClick={() => this.delete(item.key)}
      key={item.key}>{item.name}{item.title}{item.age}{item.nickname}</li>
  }

delete(key) {
  this.props.delete(key);
}

  render(){
    const todoEntries = this.props.entries;
    const ListItems = todoEntries.map(this.createTasks);

    return(
      <table>
        <thead>
          <tr className="theList">{ListItems}
            <th></th>
          </tr>
        </thead>
      </table>

      /*<ul className="theList">
        {ListItems}
      </ul>*/
    );
  }
}

export default TodoItems;
