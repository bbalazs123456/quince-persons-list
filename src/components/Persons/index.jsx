import React from 'react';
import '../../style.css';

export default function Persons(props){

 let deleteRow = (index) => {
  console.log(index)

}

  return(
    <table>
      <thead>
        <tr>
          <th>Name (job title)</th>
          <th>Age</th>
          <th>Nicname</th>
          <th>Employee</th>
          <th></th>
        </tr>
      </thead>

      <tbody>
      {
        props.data.map((row, index) =>(
          <tr key="index">
            <td>{row.name}</td>
            <td>{row.age}</td>
            <td>{row.nick}</td>
            <td>
            <label className="checkbox-style">
              <input type="checkbox"></input>
              <p className="checkmark"></p>
            </label>
            </td>
            <td className="delete-tr">
              <span id="closeBtn" onClick={deleteRow.bind(this, index)}>Delete</span>
            </td>
          </tr>
        ))
      }

      </tbody>
    </table>
  )
}
